<?php
/**
 * @file
 * Survey Points
 *
 */
 
/**
 * Admin
 *
 */ 
function surveys_points_configure($form) {

  $configure = array();

  // Get default values from variable table
  // All node types have 0 for not using surveys and 1 for using surveys
  foreach (node_get_types() as $type => $info) {
    $configure[] = _surveys_points_configure($type, $info);
  }

  $form['surveys_points'] = array(
    '#type' => 'fieldset',
    '#title' => t('Survey Points'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
    '#description' => t('The survey points will be attached to the following content types if checked.'),
  );  

  $form['surveys_points'] += $configure;

  return $form;
}


/**
 * Configure form
 *
 */
function _surveys_points_configure($type, $info) {
  $form['surveys_enabled'] = array(
    '#type' => 'hidden',
    '#title' => t(''),
    '#default_value' => variable_get('surveys_enabled_'. $type, ''),
  ); 
  $form['surveys_points_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t(''),
    '#default_value' => variable_get('surveys_points_enabled_'. $type, ''),
  ); 
  $form['type'] = array(
    '#type' => 'hidden',
    '#default_value' => $type,
  );
  $form['type_name'] = array(
    '#type' => 'markup',
    '#value' => $info->name,
  );
  if (variable_get('surveys_points_enabled_'. $type, '')) {
    $form['surveys_points_number'] = array(
      '#type' => 'select',
      '#default_value' => variable_get('surveys_points_number_' . $type, 1),
      '#options' => drupal_map_assoc(range(1, 10)),
    ); 
  }
  else {
    $form['surveys_points_number'] = array(
      '#type' => 'hidden',
      '#default_value' => 1,
    );
  }
  return $form; 
}
 