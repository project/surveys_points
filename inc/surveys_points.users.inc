<?php
/**
 * @file
 * Points/Rewards for users UI
 *
 */

/**
 * Menu callback for /user/%/surveys-points-rewards
 *
 */ 
function surveys_users(&$form_state) {
  global $user;
  
  $node = node_load(arg(1));
  
  drupal_set_title(t('%user\'s available points and rewards from filling out surveys', array('%user' => $user->name)));
  
  $form['user_points'] = array(
     '#type' => 'fieldset',
     '#title' => t('Survey Points'),
     '#collapsible' => TRUE,
     '#collapsed' => FALSE,
     '#description' => '',
     '#tree' => TRUE,
  );
  
  // Get all nodes with enabled survey points                      
  $points_for_select = surveys_points_for($user->uid);
  
  $points_from_select = surveys_points_from($user->uid);
     
  //Get questions for this node type  
  $result = db_query("SELECT 
                        sid, uid, nid, survey, points, points_claimed
                      FROM 
                        {surveys_users}
                      WHERE 
                        uid = %d", 
                      $user->uid);  

  $points = array();                                                  
  // Put categories in an array
  while ($record = db_fetch_object($result)) {
    $record->points_for_select = $points_for_select;   
    $record->points_from_select = $points_from_select;   
    $points[] = _surveys_users($record);
  } 
    
  $form['user_points'] += $points;
  
  // Make sure that only the owner can edit his own form
  if ($user->uid == $node->uid) {
    $form['submit'] = array( '#type' => 'submit', '#value' => t('Save'), '#disabled' => empty($points) ? TRUE : FALSE, ); 
  }
  else {
    drupal_set_message(t('You are not the author of this content, therefore you cannot edit its survey content.'));
  }  
  return $form;
}

/**
 * Users Form
 *
 */
function _surveys_users($record) {
  $form['sid'] = array(
    '#type' => 'hidden',
    '#value' => $record->sid,
  );
  $form['nid'] = array(
    '#type' => 'hidden',
    '#value' => $record->nid,
  );   
  $form['points'] = array(
    '#type' => 'hidden',
    '#default_value' => $record->points,
  );  
  $form['points_display'] = array(
    '#type' => 'markup',
    '#value' => $record->points,
  );    
  $form['points_for_title'] = array(
    '#type' => 'markup',
    '#return_value' => 1,
    '#value' => $record->points_for_select[$record->nid],
  );       
  $form['points_from_title'] = array(
    '#type' => 'markup',
    '#return_value' => 1,
    '#value' => $record->points_from_select[$record->survey],
  );   
  $form['points_claimed'] = array(
    '#type' => (!$form['points_claimed']['#value']) ? 'checkbox' : 'hidden',
    '#title' => t('Claimed'),
    '#return_value' => 1,
    '#default_value' => 0,
    '#description' => t(''),
  ); 
  return $form;
}

/**
 * Users Submit
 *
 */
function surveys_users_submit($form, $form_state) {
  global $user;
  $form_values = $form_state['values'];

  foreach ($form_values['points'] as $point) {
    if ($point['spid'] && $point['update'] && $point['delete'] && !$point['enabled']) {
      // Delete Points
      db_query("DELETE FROM {surveys_points} WHERE spid = %d", $point['srid']);
      drupal_set_message(t('The points: "' . $point['points_for_title'] . '" has been deleted.'));
    }
    elseif ($point['spid'] && $point['update'] && $point['delete'] && $point['enabled']) {
      // Delete Points denied
      drupal_set_message(t('You have to disable the points: "' . $point['points_for_title'] . '", to delete.'));
    }    
    elseif ($point['spid'] && $point['update'] && !$point['delete']) {
      // Update Points
      db_query("UPDATE {surveys_points} SET points = %d, enabled = %d,  points_for = %d WHERE spid = %d", $point['points'], $point['enabled'], $point['points_for'], $point['spid']);
      drupal_set_message(t('The points: "' . $point['points_for_title'] . '" has been updated.'));
    }
    elseif (!$point['spid'] && $point['points'] && $point['points_for']) {
      // Add Points to node
      db_query("INSERT INTO {surveys_points} (nid, points, points_for, enabled) VALUES (%d, %d, %d, %d)", $point['nid'], $point['points'], $point['points_for'], $point['enabled']);
      drupal_set_message(t('The point/s is added to "' . $point['points_for_title'] . '".'));
    }
    else {
      // If no condition is set, nothing should happen
    }
  }
}

/**
 * Implementation of theme_surveys_users()
 * - View in table format
 */
function theme_surveys_users($form) {
  $output = '';
  $rows = array();
  foreach (element_children($form['user_points']) as $key) {
    $row = array();
    // Strip labels 
    $form['user_points'][$key]['points_display']['#title'] = '';
    $form['user_points'][$key]['points_for_title']['#title'] = '';
    $form['user_points'][$key]['points_from_title']['#title'] = '';
    $form['user_points'][$key]['points_claimed']['#title'] = '';

    $row[] = drupal_render($form['user_points'][$key]['points_display']);
    $row[] = drupal_render($form['user_points'][$key]['points_for_title']);
    $row[] = drupal_render($form['user_points'][$key]['points_from_title']);
    $row[] = drupal_render($form['user_points'][$key]['points_claimed']);
    $rows[] = $row;
  }

  $header = array('Points', 'For', 'Surveys', 'Claim');
  // Header
  $form['user_points']['#children'] = theme('table', $header, $rows);

  $output .= drupal_render($form);
  return $output;
}
