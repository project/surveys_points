<?php
/**
 * @file
 * Survey Points Utilities
 *
 */

/**
 * Get all nodes with enabled survey points 
 *
 */
function surveys_points_for($uid) {
  foreach (node_get_types() as $type => $info) {
    if (variable_get('surveys_points_enabled_'. $type, 0)) {
      $types[] = '\'' . $type . '\'';
    }
  }

  $types = implode(',', $types);
  
  // Get all nodes created with survey points by its user
  $nodes = db_query("SELECT 
                        nid, type, title, uid
                      FROM 
                        {node}
                      WHERE 
                        uid = %d AND type IN ($types)", 
                      $uid); 
                      
  $points_for_select = array();
  $points_for_select[0] = 'none';
                                               
  // Put categories in an array
  while ($node_points = db_fetch_object($nodes)) {
    $points_for_select[$node_points->nid] = $node_points->title;
  } 
  return $points_for_select;
}                      

/**
 * Get all nodes with enabled survey
 *
 */
function surveys_points_from($uid) {
  foreach (node_get_types() as $type => $info) {
    if (variable_get('surveys_enabled_'. $type, 0)) {
      $types[] = '\'' . $type . '\'';
    }
  }

  $types = implode(',', $types);
  
  // Get all nodes created with survey points by its user
  $nodes = db_query("SELECT 
                        nid, type, title, uid
                      FROM 
                        {node}
                      WHERE 
                        uid = %d AND type IN ($types)", 
                      $uid); 
                      
  $points_for_select = array();
  $points_for_select[0] = 'none';
                                               
  // Put categories in an array
  while ($node_survey = db_fetch_object($nodes)) {
    $points_from_select[$node_survey->nid] = $node_survey->title;
  } 
  return $points_from_select;
}                

/**
 * Adds points to users for filling out surveys
 *
 */
function surveys_points_add_points($nid, $uid) {
  // Get all nodes titles associated with this survey
  $points_for_select = surveys_points_from($uid);
  
  //Get all nodes associated with this survey
  $result = db_query("SELECT 
                        nid, points, points_for, enabled
                      FROM 
                        {surveys_points}
                      WHERE 
                        nid = %d AND enabled = 1", 
                      $nid);  

  // Put categories in an array
  while ($record = db_fetch_object($result)) {
    db_query("INSERT INTO {surveys_users} (uid, nid, survey, points) VALUES (%d, %d, %d, %d)", $uid, $record->points_for, $nid, $record->points);
    drupal_set_message(t('You have earned "' . $record->points . '" on ' . $points_for_select[$record->points_for] . '.'));
  } 
}    
    
    
    
