<?php
/**
 * @file
 * Points configuration UI for each survey
 *
 */

/**
 * Menu callback for /node/%/manage-survey/points
 *
 */ 
function surveys_points(&$form_state) {
  global $user;
  
  $node = node_load(arg(1));
  
  drupal_set_title(t('Manage Points for %title', array('%title' => $node->title)));
  
  $form['points'] = array(
     '#type' => 'fieldset',
     '#title' => t('Survey Points'),
     '#collapsible' => TRUE,
     '#collapsed' => FALSE,
     '#description' => '',
     '#tree' => TRUE,
  );
  // Get all node types with enabled survey points
  foreach (node_get_types() as $type => $info) {
    if (variable_get('surveys_points_enabled_'. $type, 0)) {
      $types[] = '\'' . $type . '\'';
    }
  }

  $types = implode(',', $types);
  
  // Get all nodes created with survey points by its user
  $nodes = db_query("SELECT 
                        nid, type, title, uid
                      FROM 
                        {node}
                      WHERE 
                        uid = %d AND type IN ($types)", 
                      $user->uid); 
                      
  $points_for_select = array();
  $points_for_select[0] = 'none';                                                  
  // Put categories in an array
  while ($node_points = db_fetch_object($nodes)) {
    $points_for_select[$node_points->nid] = $node_points->title;
  } 
                        
   
  //Get questions for this node type  
  $result = db_query("SELECT 
                        spid, nid, points, points_for, enabled
                      FROM 
                        {surveys_points}
                      WHERE 
                        nid = %d", 
                      $node->nid);  

  $points = array();                                                  
  // Put categories in an array
  while ($record = db_fetch_object($result)) {
    $record->nid = $node->nid;
    $record->points_for_select = $points_for_select;   
    $points[] = _surveys_points($record);
  } 
  
  // add another blank question fields
  $record = new stdClass();
  $record->spid = 0;
  $record->nid = $node->nid;    
  $record->points_for_select = $points_for_select;


  // This function will add additional blank rows
  surveys_blank_rows(&$points, 1, $record, 'points');
    
  $form['points'] += $points;
  
  // Make sure that only the owner can edit his own form
  if ($user->uid == $node->uid) {
    $form['submit'] = array( '#type' => 'submit', '#value' => t('Save Points'), '#disabled' => empty($points) ? TRUE : FALSE, ); 
  }
  else {
    drupal_set_message(t('You are not the author of this content, therefore you cannot edit its survey content.'));
  }  
  return $form;
}

/**
 * Points Form
 *
 */
function _surveys_points($record) {
  $form['spid'] = array(
    '#type' => 'hidden',
    '#value' => $record->spid,
  );
  $form['nid'] = array(
    '#type' => 'hidden',
    '#value' => $record->nid,
  ); 
  $form['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#return_value' => 1,
    '#default_value' => (boolean)($record->enabled),
    '#description' => t(''),
  );
  $form['update'] = array(
    '#type' => ($form['spid']['#value']) ? 'checkbox' : 'hidden',
    '#title' => t('Update'),
    '#return_value' => 1,
    '#default_value' => 0,
    '#description' => t(''),
  );  
  $form['points'] = array(
    '#type' => 'textfield',
    '#return_value' => 1,
    '#size' => 10,
    '#default_value' => $record->points,
    '#description' => '',
  );  
  $form['points_for'] = array(
    '#type' => 'select',
    '#return_value' => 1,
    '#default_value' => $record->points_for,
    '#options' => $record->points_for_select,
    '#description' => '',
  );    
  $form['points_for_title'] = array(
    '#type' => 'hidden',
    '#default_value' => $record->points_for_select[$record->points_for],

  );    
       
  $form['delete'] = array(
    '#type' => ($form['spid']['#value']) ? 'checkbox' : 'hidden',
    '#title' => t('Delete'),
    '#return_value' => 1,
    '#default_value' => 0,
    '#description' => t(''),
  ); 
  return $form;
}

/**
 * Points Submit
 *
 */
function surveys_points_submit($form, $form_state) {
  global $user;
  $form_values = $form_state['values'];

  foreach ($form_values['points'] as $point) {
    if ($point['spid'] && $point['update'] && $point['delete'] && !$point['enabled']) {
      // Delete Points
      db_query("DELETE FROM {surveys_points} WHERE spid = %d", $point['srid']);
      drupal_set_message(t('The points: "' . $point['points_for_title'] . '" has been deleted.'));
    }
    elseif ($point['spid'] && $point['update'] && $point['delete'] && $point['enabled']) {
      // Delete Points denied
      drupal_set_message(t('You have to disable the points: "' . $point['points_for_title'] . '", to delete.'));
    }    
    elseif ($point['spid'] && $point['update'] && !$point['delete']) {
      // Update Points
      db_query("UPDATE {surveys_points} SET points = %d, enabled = %d,  points_for = %d WHERE spid = %d", $point['points'], $point['enabled'], $point['points_for'], $point['spid']);
      drupal_set_message(t('The points: "' . $point['points_for_title'] . '" has been updated.'));
    }
    elseif (!$point['spid'] && $point['points'] && $point['points_for']) {
      // Add Points to node
      db_query("INSERT INTO {surveys_points} (nid, points, points_for, enabled) VALUES (%d, %d, %d, %d)", $point['nid'], $point['points'], $point['points_for'], $point['enabled']);
      drupal_set_message(t('The point/s is added to "' . $point['points_for_title'] . '".'));
    }
    else {
      // If no condition is set, nothing should happen
    }
  }
}

/**
 * Implementation of theme_surveys_points()
 * - View in table format
 */
function theme_surveys_points($form) {
  $output = '';
  $rows = array();
  foreach (element_children($form['points']) as $key) {
    $row = array();
    // Strip labels 
    $form['points'][$key]['enabled']['#title'] = '';
    $form['points'][$key]['update']['#title'] = '';
    $form['points'][$key]['points']['#title'] = '';
    $form['points'][$key]['points_for']['#title'] = '';
    $form['points'][$key]['delete']['#title'] = '';                
    $row[] = drupal_render($form['points'][$key]['enabled']);
    $row[] = drupal_render($form['points'][$key]['update']);
    $row[] = drupal_render($form['points'][$key]['points']);
    $row[] = drupal_render($form['points'][$key]['points_for']);
    $row[] = drupal_render($form['points'][$key]['delete']);                
    $rows[] = $row;
  }

  $header = array('Enabled', 'Update / Save', 'Points', 'Points for', 'Delete');
  // Header
  $form['points']['#children'] = theme('table', $header, $rows);

  $output .= drupal_render($form);
  return $output;
}
