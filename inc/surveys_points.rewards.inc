<?php
/**
 * @file
 * Points/Rewards configuration UI
 *
 */

/**
 * Menu callback for /node/%/surveys-rewards
 *
 */ 
function surveys_points_rewards(&$form_state) {
  global $user;
  
  $node = node_load(arg(1));
  
  drupal_set_title(t('Manage Points and Rewards for %title', array('%title' => $node->title)));
  
  $form['rewards'] = array(
    '#type' => 'fieldset',
    '#title' => t('Survey Rewards'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#description' => '',
    '#tree' => TRUE,
  );
    
  //Get questions for this node type  
  $result = db_query("SELECT 
                        srid, nid, points, reward, enabled
                      FROM 
                        {surveys_rewards} 
                      WHERE 
                        nid = %d
                      ORDER BY 
                        points DESC", 
                      $node->nid);  

  $points = array();                                                  
  // Put categories in an array
  while ($record = db_fetch_object($result)) {
    $record->nid = $node->nid;
    $points[] = _surveys_points_rewards($record);
  } 

  // add another blank question fields
  $record = new stdClass();
  $record->srid = 0;
  $record->nid = $node->nid;  
  // Get settings for the number of blank rows
  $rows_blank = variable_get('surveys_points_number_' . $node->type, 1);
  
  // This function will add additional blank rows
  surveys_blank_rows(&$points, $rows_blank, $record, 'points_rewards');

  $form['rewards'] += $points;

  // Make sure that only the owner can edit his own form
  if ($user->uid == $node->uid) {
    $form['submit'] = array( '#type' => 'submit', '#value' => t('Save'), '#disabled' => empty($points) ? TRUE : FALSE, ); 
  }
  else {
    drupal_set_message(t('You are not the author of this content, therefore you cannot edit its survey content.'));
  }  
  return $form;
}

/**
 * Rewards Form
 *
 */
function _surveys_points_rewards($record) { 
  $form['srid'] = array(
    '#type' => 'hidden',
    '#value' => $record->srid,
  ); 
  $form['nid'] = array(
    '#type' => 'hidden',
    '#value' => $record->nid,
  );   
  $form['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#return_value' => 1,
    '#default_value' => (boolean)($record->enabled),
    '#description' => t(''),
  );  
  $form['points'] = array(
    '#type' => 'textfield',
    '#title' => t('Points'),
    '#size' => 10,
    '#return_value' => 1,
    '#default_value' => $record->points,
    '#description' => t(''),
  );
  $form['reward'] = array(
    '#type' => 'textfield',
    '#title' => t('Reward'),
    '#return_value' => 1,
    '#default_value' => $record->reward,
    '#description' => t(''),
  );
  $form['update'] = array(
    '#type' => ($form['srid']['#value']) ? 'checkbox' : 'hidden',
    '#title' => t('Update'),
    '#return_value' => 1,
    '#default_value' => 0,
    '#description' => t(''),
  );
  $form['delete'] = array(
    '#type' => ($form['srid']['#value']) ? 'checkbox' : 'hidden',
    '#title' => t('Delete'),
    '#return_value' => 1,
    '#default_value' => 0,
    '#description' => t(''),
  );  
  
  return $form;
}

/**
 * Rewards Submit
 *
 */
function surveys_points_rewards_submit($form, $form_state) {
  $form_values = $form_state['values'];

  foreach ($form_values['rewards'] as $reward) {   
    if ($reward['srid'] && $reward['update'] && $reward['delete'] && !$reward['enabled']) {
      // Delete Points/Rewards
      db_query("DELETE FROM {surveys_rewards} WHERE srid = %d", $reward['srid']);
      drupal_set_message(t('The reward: "' . $reward['reward'] . '" has been deleted.'));
    }
    elseif ($reward['srid'] && $reward['update'] && $reward['delete'] && $reward['enabled']) {
      // Delete Points/Rewards denied
      drupal_set_message(t('You have to disable the reward: "' . $reward['reward'] . '", to delete.'));
    }    
    elseif ($reward['srid'] && $reward['update'] && !$reward['delete']) {
      // Update Points/Rewards
      db_query("UPDATE {surveys_rewards} SET points = %d, enabled = %d,  reward ='%s' WHERE srid = %d", $reward['points'], $reward['enabled'], $reward['reward'], $reward['srid']);
      drupal_set_message(t('The reward: "' . $reward['reward'] . '" has been updated.'));
    }
    elseif (!$reward['srid'] && $reward['points'] && $reward['reward']) {
      // Add new Points/Rewards
      db_query("INSERT INTO {surveys_rewards} (nid, points, reward, enabled) VALUES (%d, %d, '%s', %d)", $reward['nid'], $reward['points'], $reward['reward'], $reward['enabled']);
      drupal_set_message(t('The settings for this points and rewards have been saved.'));
    }
    else {
      // If no condition is set, nothing should happen
    }
  }
}

/**
 * Implementation of theme_surveys_points_rewards()
 * - View in table format
 */
function theme_surveys_points_rewards($form) {
  $output = '';
  $rows = array();
  foreach (element_children($form['rewards']) as $key) {
    $row = array();
    // Strip labels 
    $form['rewards'][$key]['enabled']['#title'] = '';
    $form['rewards'][$key]['update']['#title'] = '';
    $form['rewards'][$key]['points']['#title'] = '';
    $form['rewards'][$key]['reward']['#title'] = '';
    $form['rewards'][$key]['delete']['#title'] = '';
    $row[] = drupal_render($form['rewards'][$key]['enabled']);
    $row[] = drupal_render($form['rewards'][$key]['update']);
    $row[] = drupal_render($form['rewards'][$key]['points']);
    $row[] = drupal_render($form['rewards'][$key]['reward']);
    $row[] = drupal_render($form['rewards'][$key]['delete']);
    $rows[] = $row;
  }
  $header = array('Enabled', 'Update / Save', 'Points', 'Rewards', 'Delete');
  // Header
  $form['rewards']['#children'] = theme('table', $header, $rows);

  $output .= drupal_render($form);
  return $output;
}


